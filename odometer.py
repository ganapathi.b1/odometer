def is_ascending(num: int) -> bool:
  numlist= list(str(num))
  return sorted(numlist) == numlist
def size(n: int) -> int:
    return len(str(n))
def get_limits(n: int) -> tuple[int, int]:
  DIGITS = "123456789"
  k = size(n)
  return int(DIGITS[:k]) , int(DIGITS[-k:])
def next_reading(n: int, k = 1) -> int:
    START, LIMIT = get_limits(n)
    for i in range(k):
        if n == LIMIT:
            n = START
        else:
            n += 1
            while not is_ascending(n):
                n += 1
    return n

def prev_reading(n: int, k = 1) -> int:
    START, LIMIT = get_limits(n)
    for j in range(k):
        if n == START:
            n = LIMIT
        else:
            n -= 1
            while not is_ascending(n):
                n -= 1
    return n

def distance(a_reading: int, b_reading: int) -> int:
    if size(a_reading) != size(b_reading):
        return -1
    steps = 0
    while a_reading != b_reading:
        steps += 1
        a_reading = next_reading(a_reading)
    return steps

def next_kth_reading(n: int, k: int) -> int:
    for _ in range(k):
        n = next_reading(n)
    return n
def prev_kth_reading(n: int, k: int) -> int:
    for _ in range(k):
        n = prev_reading(n)
    return n

prev_reading(344)
